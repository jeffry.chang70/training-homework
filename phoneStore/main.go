package main

import (
	"fmt"
	"phoneStore/Mapper"
	"phoneStore/Phone"
)

func main() {
	var menu int
	menuSelected := "Sorry Menu Does not exist"
	fmt.Println("Welcome to Jakarta Phone Store")
	fmt.Println("Enter 1 for view all product list")
	fmt.Println("Enter 2 for insert new product in listing")
	fmt.Println("Enter 3 for testing public struct private variable ")
	fmt.Println("Enter 0 for exit")
	fmt.Print("Enter menu number: ")
	fmt.Scanf("%d", &menu)
	for {
		if menu == 0 {
			menuSelected = "------Thank you come again------"
			fmt.Println(menuSelected)
			break
		} else if menu == 1 {
			fmt.Println("-----All product list-----")
			Phone.ViewAllPhone()
			menuSelected = "------End of product list------"
			fmt.Println(menuSelected)
			break
		} else if menu == 2 {
			menuSelected = "------Insert Phone Product-----"
			fmt.Println(menuSelected)
			inserted := Phone.NewPhone(
				"Xiaomi",
				200000,
				"Jakarta Selatan",
				99,
			)
			fmt.Println(inserted)
			Phone.ViewAllPhone()
			fmt.Println("-----End of insert Phone product-----")
			break
			//test private public with getter setter
		} else if menu == 3 {
			menuSelected = "------Insert Phone Product with Specs-----"
			fmt.Println(Mapper.Add())
			fmt.Println("-----End of insert Phone with Specs-----")
			break
		}
		fmt.Println(menuSelected)
		break
	}

}
