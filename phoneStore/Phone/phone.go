package Phone

import "fmt"

type PhoneDisplay struct {
	PhoneBrand  string
	PhonePrice  int
	PhoneStatus string
	Specs       Specifications
	Warehouse   []Warehouse
}

type Specifications struct {
	operatingSystem string
	phoneColor      string
	phoneNetwork    string
}

type Warehouse struct {
	WarehouseAddress string
	WarehouseStock   int
}

func ViewAllPhone() {
	//emptyStock := "currently unavailable"
	//zeroWarehouse := Warehouse{}
	phone := PhoneDisplay{
		PhoneBrand:  "ZIFO",
		PhonePrice:  100000,
		PhoneStatus: "In Stock",
	}
	var phoneWareHouse Warehouse
	phoneWareHouse.WarehouseAddress = "Jakarta Barat"
	phoneWareHouse.WarehouseStock = 50
	phone.Warehouse = append(phone.Warehouse, phoneWareHouse)
	phoneWareHouse.WarehouseAddress = "Jakarta Timur"
	phoneWareHouse.WarehouseStock = 20
	phone.Warehouse = append(phone.Warehouse, phoneWareHouse)
	phoneWareHouse.WarehouseAddress = "Jakarta Pusat"
	phoneWareHouse.WarehouseStock = 100
	phone.Warehouse = append(phone.Warehouse, phoneWareHouse)
	fmt.Println(phone)
	//if (Warehouse{}) == zeroWarehouse {
	//	fmt.Println(emptyStock)
	//} else if (Warehouse{} != zeroWarehouse) {
	//}
}

func NewPhone(PhoneBrand string, PhonePrice int, PhoneWarehouseAddress string, PhoneStock int) *PhoneDisplay {
	//var price int
	//var phoneWareHouse Warehouse
	//fmt.Scanf("%s", phoneWareHouse.WarehouseStock)
	insertPhone := PhoneDisplay{
		PhoneBrand:  PhoneBrand,
		PhonePrice:  PhonePrice,
		PhoneStatus: "In Stock",
	}
	var phoneWareHouse Warehouse
	phoneWareHouse.WarehouseAddress = PhoneWarehouseAddress
	phoneWareHouse.WarehouseStock = PhoneStock
	insertPhone.Warehouse = append(insertPhone.Warehouse, phoneWareHouse)
	//	PhonePrice:  price,
	//	PhoneStatus: "In Stock",
	//}
	//insertPhone.Warehouse = append(insertPhone.Warehouse, phoneWareHouse)
	return &insertPhone
}

func (p *Specifications) SetMappingSpecs(operatingSystem, phoneColor, phoneNetwork string) {
	p.operatingSystem = operatingSystem
	p.phoneColor = phoneColor
	p.phoneNetwork = phoneNetwork
}

func (p *Specifications) GetMappingSpecs() *Specifications {
	return p
}
