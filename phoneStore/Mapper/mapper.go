package Mapper

import (
	"fmt"
	"phoneStore/Phone"
)

func Add() Phone.PhoneDisplay {
	var phone Phone.PhoneDisplay
	phone.PhoneBrand = "POPO"
	phone.PhonePrice = 89000
	phone.PhoneStatus = "In Stock"
	phone.Specs.SetMappingSpecs("Alpha OS", "Black", "5G")
	fmt.Println(phone.Specs.GetMappingSpecs())
	return phone
}
